# DieBahn

A GTK4 frontend for the travel information of the german railway ("Reiseauskunft der Deutschen Bahn") and many more.

## Screenshots

![Overview of the application](https://gitlab.com/Schmiddiii/diebahn/-/raw/master/data/screenshots/overview.png)

## Features

- Search for a journey.
- List all available journeys.
- View the details of a journey including departure, arrivals, delays, platforms.
- Convergent for small screens.
- Bookmark a search or a journey.
- Show more information like prices.
- Many different search profiles, e.g.
    - DB
    - SNCF
    - ÖBB
    - BART
    - ... (36 more). For a full list of supported profiles, see [hafas-rs](https://gitlab.com/Schmiddiii/hafas-rs#profiles) which is used to query the application data.

## Contact

There is a matrix room setup for this application. You can join it [here](https://matrix.to/#/%23diebahn:matrix.org?via=matrix.org) if you want.
