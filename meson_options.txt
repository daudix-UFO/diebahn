option(
  'profile',
  type: 'combo',
  choices: [
    'default',
    'development',
  ],
  value: 'default',
  description: 'The build profile for DieBahn. One of "default" or "development".'
)
